package ro.pub.cs.systems.eim.exemplu_colocviu

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import ro.pub.cs.systems.eim.exemplu_colocviu.Constants.COUNTER_1
import ro.pub.cs.systems.eim.exemplu_colocviu.Constants.COUNTER_2

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val counterClass = CounterClass()
        setContent {
            ColocviuUI(counterClass)
        }

    }
}

class CounterClass {

    var counter1 = mutableIntStateOf(0)
    var counter2 = mutableIntStateOf(0)

    fun increment1 () {
        counter1.intValue += 1
    }

    fun increment2 () {
        counter2.intValue += 1
    }

}

@Composable
fun ColocviuUI(myClass : CounterClass) {
    val context = LocalContext.current

    val counter1 = myClass.counter1.intValue
    val counter2 by rememberSaveable {
        myClass.counter2
    }

    val activityResultsLauncher = rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult())
    {result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val sum = result.data?.getIntExtra("res", 0)
            Toast.makeText(context, "The activity returned with result OK: $sum", Toast.LENGTH_LONG).show()
        }
        else if (result.resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(context, "The activity returned with result CANCELED", Toast.LENGTH_LONG).show()
        }
    }

    Column {
        Button(
            onClick = {
                val intent = Intent(context, SecondaryActivity::class.java)
                intent.putExtra(COUNTER_1, counter1)
                intent.putExtra(COUNTER_2, counter2)
                activityResultsLauncher.launch(intent)
            },
            modifier = Modifier
                .align(Alignment.End)
                .padding(24.dp)
        ) {
            Text(text = "Navigate to secondary activity")
        }
        Row {

            OutlinedTextField(value = "count 1: $counter1", onValueChange = {}, modifier = Modifier
                .weight(1f)
                .padding(start = 8.dp))
            Spacer(modifier = Modifier.padding(8.dp))
            OutlinedTextField(value = "count 2: $counter2", onValueChange = {}, modifier = Modifier
                .weight(1f)
                .padding(end = 8.dp))
        }
        Row {
            Button(
                onClick = {myClass.increment1()},
                modifier = Modifier
                    .weight(1f)
                    .padding(16.dp))
            {
                Text("Press me")
            }

            Spacer(modifier = Modifier.padding(16.dp))
            Button(
                onClick = {myClass.increment2()},
                modifier = Modifier
                    .weight(1f)
                    .padding(16.dp))
            {
                Text("Press me too")

            }

        }

    }

}

