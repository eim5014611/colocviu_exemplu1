package ro.pub.cs.systems.eim.exemplu_colocviu

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ro.pub.cs.systems.eim.exemplu_colocviu.Constants.COUNTER_1
import ro.pub.cs.systems.eim.exemplu_colocviu.Constants.COUNTER_2

class SecondaryActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var sum = 0
        val counter1 = intent.getIntExtra(COUNTER_1, 0)
        val counter2 = intent.getIntExtra(COUNTER_2, 0)
        sum = counter1 + counter2
        setContent {

            Column {
                Text(text = "The sum is: $sum", modifier = Modifier.padding(16.dp))
                Row {
                    Button(
                        onClick = {
                            setResult(RESULT_OK, intent.putExtra("res", sum))
                            finish()
                        },
                        modifier = Modifier
                            .weight(1f)
                            .padding(16.dp)
                    )
                    {
                        Text("OK")
                    }
                    Button(
                        onClick = {
                            setResult(RESULT_CANCELED)
                            finish()
                        },
                        modifier = Modifier
                            .weight(1f)
                            .padding(16.dp)
                    )
                    {
                        Text("CANCEL")
                    }
                }
            }


        }
    }
}

